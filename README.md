Prova Analista Júnior

 Tecnologias

Codeigniter(resolvi aprender já que vocês usam)

BootStrap

PHP


Instruções:

1 - Entrar no xampp e ligar o apache e o mysql.

2 - fazer o import do banco(phpmyadmin) que esta em: app/database/demo.sql.

3 - Rodar o comando "php spark serve" dentro de \ThiagoFariaRodrigues

4 - Acessar o http://localhost:8080/index.php/users-list a partir do navegador.

Link de um video rodando as funcionalidades básicas: https://youtu.be/IkRVHExLDys


O que faltou fazer(infelizmente pude dedicar apenas cerca de 7h para a prova):

Implementar autenticação de usuário na aplicação.
API Rest JSON para todos os CRUDS .
Ajustar o programa pra chave estrangeira entre a tabela de curso e aluno ter o comportamento esperado .


Duvidas: thiagorodrigues@email.com  -  (61)999320640
