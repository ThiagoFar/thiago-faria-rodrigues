<?php 
namespace App\Models;
use CodeIgniter\Model;

class CursoModel extends Model
{
    protected $table = 'cursos';

    protected $primaryKey = 'codigo';
    
    protected $allowedFields = ['nomeCurso'];
}