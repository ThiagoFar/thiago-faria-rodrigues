<!DOCTYPE html>
<html>

<head>
  <title>Prova Analista</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  <style>
    .container {
      max-width: 500px;
    }

    .error {
      display: block;
      padding-top: 5px;
      font-size: 14px;
      color: red;
    }
  </style>
</head>

<body>
  <div class="container mt-5">
    <form method="post" id="update_userc" name="update_userc" 
    action="<?= site_url('/updatec') ?>">
      <input type="hidden" name="codigo" id="codigo" value="<?php echo $user_obj['codigo']; ?>">

      <div class="form-group">
        <label>Nome do Curso</label>
        <input type="text" name="nomeCurso" class="form-control" value="<?php echo $user_obj['nomeCurso']; ?>">
      </div>

    

      <div class="form-group">
        <button type="submit" class="btn btn-danger btn-block">Salvar</button>
      </div>
    </form>
  </div>

  <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>
  <script>
    if ($("#update_user").length > 0) {
      $("#update_user").validate({
        rules: {
          name: {
            required: true,
          },
          
        },
        messages: {
          name: {
            required: "Name is required.",
          },
         
        },
      })
    }
  </script>
</body>

</html>