<!doctype html>
<html lang="en">
  <head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Prova Analista</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
</head>
<body>
  <div class="container mt-4">
  <h1> Cursos</h1>
  <div class="container mt-4">
    <div class="d-flex justify-content-end">
        <a href="<?php echo site_url('/users-list') ?>" class="btn btn-success mb-2">Listar Alunos</a>
  </div>

    <div class="d-flex justify-content-end">
        <a href="<?php echo site_url('/curso-form') ?>" class="btn btn-success mb-2">Adicionar Curso</a>
  </div>
  
    <?php
     if(isset($_SESSION['msg'])){
        echo $_SESSION['msg'];
      }
     ?>
  <div class="mt-3">
     <table class="table table-bordered" id="curso-list">
       <thead>
          <tr>
             <th>Código</th>
             <th>Nome</th>
             <th>Ações</th>
          </tr>
       </thead>
       <tbody>
          <?php if($cursos): ?>
          <?php foreach($cursos as $curso): ?>
          <tr>
             <td><?php echo $curso['codigo']; ?></td>
             <td><?php echo $curso['nomeCurso']; ?></td>
            
             <td>
              <a href="<?php echo base_url('edit-viewc/'.$curso['codigo']);?>" class="btn btn-primary btn-sm">Editar</a>
              <a href="<?php echo base_url('deletec/'.$curso['codigo']);?>" class="btn btn-danger btn-sm">Deletar</a>
              </td>
          </tr>
         <?php endforeach; ?>
         <?php endif; ?>
       </tbody>
     </table>
  </div>
</div>
 
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
<script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready( function () {
      $('#curso-list').DataTable({
          "language": {
      "url": "http://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Portuguese-Brasil.json"
  },
  "pageLength": 5

 
});

  } );
</script>
</body>
</html>