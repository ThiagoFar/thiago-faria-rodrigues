<!DOCTYPE html>
<html>

<head>
  <title>Prova Analista</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  <style>
    .container {
      max-width: 500px;
    }

    .error {
      display: block;
      padding-top: 5px;
      font-size: 14px;
      color: red;
    }
  </style>
</head>

<body>
  <div class="container mt-5">
    <form method="post" id="update_user" name="update_user" 
    action="<?= site_url('/update') ?>">
      <input type="hidden" name="id" id="id" value="<?php echo $user_obj['id']; ?>">

      <div class="form-group">
        <label>Name</label>
        <input type="text" name="name" class="form-control" value="<?php echo $user_obj['name']; ?>">
      </div>

      <div class="form-group">
        <label>endereco</label>
        <input type="text" name="endereco" class="form-control" value="<?php echo $user_obj['endereco']; ?>">
      </div>

      <div class="form-group">
        <label>Curso</label>
        <select name="nomeCurso" id="nomeCurso" class="form-control">>
        <option selected="selected "value="<?php echo $user_obj['nomeCurso'];?>"><?php echo $user_obj['nomeCurso'];?></option>
        <option value="Ciência da computação">Ciência da computação</option>
        <option value="Análise e desenvolvimento de sistemas">Análise e desenvolvimento de sistemas</option>
        <option value="Física">Física</option>
        <option value="Engenharia de Software">Engenharia de Software</option>
        <option value="Matemática">Matemática</option>
        <option value="Química">Química</option>
        <option value="Direito">Direito</option>
        <option value="Pscicologia">Pscicologia</option>
        <option value="Ciência Política">Ciência Política </option>
        <option value="Artes">Artes</option>
        <option value="Arquitetura">Arquitetura</option>
        <option value="Design">Design</option>
        <option value="Medicina">Medicina</option>
        <option value="Enfermagem">Enfermagem</option>
        <option value="Gastronomia">Gastronomia</option>
        </select>
      </div>


      <div class="form-group">
        <button type="submit" class="btn btn-danger btn-block">Salvar</button>
      </div>
    </form>
  </div>

  <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>
  <script>
    if ($("#update_user").length > 0) {
      $("#update_user").validate({
        rules: {
          name: {
            required: true,
          },
          
        },
        messages: {
          name: {
            required: "Name is required.",
          },
         
        },
      })
    }
  </script>
</body>

</html>