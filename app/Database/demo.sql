-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 02-Maio-2021 às 18:16
-- Versão do servidor: 10.4.18-MariaDB
-- versão do PHP: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `demo`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `cursos`
--

CREATE TABLE `cursos` (
  `codigo` int(11) NOT NULL,
  `nomeCurso` varchar(45) COLLATE latin1_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

--
-- Extraindo dados da tabela `cursos`
--

INSERT INTO `cursos` (`codigo`, `nomeCurso`) VALUES
(1, 'Ciência da computação'),
(2, 'Engenharia de Software'),
(3, 'Análise e desenvolvimento de sistemas'),
(4, 'Engenharia da Computação'),
(5, 'Física'),
(6, 'Matemática'),
(7, 'Química '),
(8, 'Direito'),
(9, 'Pscicologia'),
(10, 'Ciência Política '),
(11, 'Artes'),
(12, 'Arquitetura'),
(13, 'Design'),
(14, 'Medicina'),
(15, 'Enfermagem'),
(16, 'Gastronomia');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL COMMENT 'Primary Key',
  `name` varchar(100) NOT NULL COMMENT 'Name',
  `endereco` varchar(255) NOT NULL COMMENT 'Email Address',
  `nomeCurso` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='datatable demo table';

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `name`, `endereco`, `nomeCurso`) VALUES
(5, 'Sova', 'Moscow Russia casa 14', 'Ciência da computação'),
(6, 'Phoenix', 'Londres Inglaterra casa 25', 'Análise e desenvolvimento de sistemas'),
(8, 'Brimstone', 'Nova York EUA casa 77', 'Física'),
(9, 'Sage', 'Hong Kong China casa 15', 'Engenharia de Software'),
(11, 'Cypher', 'Rabat Marrocos casa 49', 'Engenharia da Computação'),
(14, 'Viper', 'Los Angeles EUA casa 45', 'Engenharia de Software'),
(15, 'Reyna', 'Cidade do México ', 'Análise e desenvolvimento de sistemas'),
(16, 'Killjoy', 'Bonn Alemanha casa 27', 'Design'),
(17, 'Breach', 'Estocolmo Suécia  casa 22', 'Arquitetura'),
(18, 'Jett', 'Seul Coreia do sul casa 55', 'Medicina'),
(19, 'Skye', 'Sydney Austrália casa 13', 'Ciência da computação');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `cursos`
--
ALTER TABLE `cursos`
  ADD PRIMARY KEY (`codigo`);

--
-- Índices para tabela `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `cursos`
--
ALTER TABLE `cursos`
  MODIFY `codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de tabela `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key', AUTO_INCREMENT=20;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
