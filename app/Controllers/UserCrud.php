<?php 
namespace App\Controllers;
use App\Models\UserModel;
use App\Models\CursoModel;
use CodeIgniter\Controller;

class UserCrud extends Controller
{
    // show users list
    public function index(){
        $userModel = new UserModel();
        $data['users'] = $userModel->orderBy('id', 'DESC')->findAll();
        return view('user_view', $data);
    }

    

    public function curso(){
        $cursoModel = new CursoModel();
        $data['cursos'] = $cursoModel->orderBy('codigo', 'DESC')->findAll();
        return view('curso_view', $data);
    }

    // add user form
    public function create(){
        return view('add_user');
    }
    public function addCurso(){
        return view('add_curso');
    }
 
    // insert data
    public function store() {
        $userModel = new UserModel();
        $data = [
            'name' => $this->request->getVar('name'),
            'endereco'  => $this->request->getVar('endereco'),
            'nomeCurso'  => $this->request->getVar('nomeCurso'),
        ];
        $userModel->insert($data);
        return $this->response->redirect(site_url('/users-list'));
    }

    // insert data
    public function storec() {
        $cursoModel = new CursoModel();
        $data = [
            'nomeCurso' => $this->request->getVar('nomeCurso')
           
        ];
        $cursoModel->insert($data);
        return $this->response->redirect(site_url('/curso-list'));
    }

    // show single user
    public function singleUser($id = null){
        $userModel = new UserModel();
        $data['user_obj'] = $userModel->where('id', $id)->first();
        return view('edit_user', $data);
    }

     // show single user
    public function singleUserc($id = null){
        $cursoModel = new CursoModel();
        $data['user_obj'] = $cursoModel->where('codigo', $id)->first();
        return view('edit_curso', $data);
    }

    // update user data
    public function update(){
        $userModel = new UserModel();
        $id = $this->request->getVar('id');
        $data = [
            'name' => $this->request->getVar('name'),
            'endereco'  => $this->request->getVar('endereco'),
            'nomeCurso'  => $this->request->getVar('nomeCurso'),
        ];
        $userModel->update($id, $data);
        return $this->response->redirect(site_url('/users-list'));
    }

    // update user data
    public function updatec(){
        $cursoModel = new CursoModel();
        $id = $this->request->getVar('codigo');
        $data = [
            'nomeCurso' => $this->request->getVar('nomeCurso')
            
        ];
        $cursoModel->update($id, $data);
        return $this->response->redirect(site_url('/curso-list'));
    }
 
    // delete user
    public function delete($id = null){
        $userModel = new UserModel();
        $data['user'] = $userModel->where('id', $id)->delete($id);
        return $this->response->redirect(site_url('/users-list'));
    }    

    // delete curso
    public function deletec($id = null){
        $cursoModel = new CursoModel();
        $data['curso'] = $cursoModel->where('codigo', $id)->delete($id);
        return $this->response->redirect(site_url('/curso-list'));
    }    
}